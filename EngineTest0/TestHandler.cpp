#include "TestHandler.h"
#include "TestScreen.h"


TestHandler::TestHandler() : IScreenHandler() {
	
}


TestHandler::~TestHandler() {

}

void TestHandler::Init() {
	m_vertShaderPath = "Data/Shaders/colorShading.vert";
	m_fragShaderPath = "Data/Shaders/colorShading.frag";
	m_screenWidth = 1280;
	m_screenHeight = 720;
	m_windowName = "Vodka Engine Test";
	m_screenFlags = 0b000;

	m_screens.emplace_back(std::make_pair(new TestScreen(this), "testScreen"));

	m_fontPath = "Data/Fonts/kremlin.ttf";
	m_fontSize = 64;
}
