// EngineTest.cpp : Defines the entry point for the console application.
//
#ifdef __cplusplus
  extern "C"
#endif
#include "TestHandler.h"

int main(int argc, char* argv[]) {
	TestHandler* test = new TestHandler();

	test->Run();

	delete test;

    return 0;
}

