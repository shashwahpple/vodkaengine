#include "TestScreen.h"
#include <Camera2D.h>
#include <TextBehaviour.h>

TestScreen::TestScreen(VodkaEngine::IScreenHandler* handler) : VodkaEngine::IScreen(handler) {
	
}

TestScreen::~TestScreen() {
	delete m_spriteBatch;
	delete m_testObj;
}

void TestScreen::Init() {
	m_spriteBatch = new VodkaEngine::Spritebatch();
	m_spriteBatch->Init();

	VodkaEngine::Camera2D* camera = new VodkaEngine::Camera2D();
	camera->Init(m_screenHandler->GetWidth(), m_screenHandler->GetHeight());
	camera->SetPosition(glm::vec2(float(m_screenHandler->GetWidth()) / 2.0f, float(m_screenHandler->GetHeight()) / 2.0f));

	m_activeCameras.emplace_back(camera);

	m_testObj = new VodkaEngine::GameObject();

	VodkaEngine::TextBehaviour* text = (VodkaEngine::TextBehaviour*)m_testObj->AddBehaviour(new VodkaEngine::TextBehaviour(m_screenHandler->GetFontBatch()));
	text->SetOffset(glm::vec2(100.0f, 100.0f));
	text->SetText("Hello, world!\nWelcome to Vodka!");

	m_screenHandler->GetAudio()->LoadMusic("Data/Sound/Music/A Free Meal pt.ogg").Play();
	m_screenHandler->GetAudio()->SetVolume(32);
}

void TestScreen::Update(float deltaTime) {

}

void TestScreen::Draw() {
	m_spriteBatch->Begin(VodkaEngine::GlyphSortType::FRONT_TO_BACK);

	m_testObj->Draw(m_spriteBatch);

	m_spriteBatch->End();
	m_spriteBatch->RenderBatch();
}