#pragma once
#include <IScreen.h>
#include <Spritebatch.h>
#include <GameObject.h>
#include <SpriteBehaviour.h>
#include <FontBatch.h>

class TestScreen : public VodkaEngine::IScreen {
public:
	TestScreen(VodkaEngine::IScreenHandler* handler);
	~TestScreen();

	virtual void Init() override;
	virtual void Update(float deltaTime) override;

protected:
	virtual void Draw() override;

	VodkaEngine::Spritebatch* m_spriteBatch;
	VodkaEngine::GameObject* m_testObj;
};

