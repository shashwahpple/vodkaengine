#pragma once
#include "IBehaviour.h"
#include "FontBatch.h"

namespace VodkaEngine {
	class VODKA_API TextBehaviour : public IBehaviour {
	public:
		TextBehaviour(FontBatch* fontBatch);
		~TextBehaviour();

		virtual void Draw(Spritebatch* spritebatch) override;

		void SetOffset(glm::vec2& pos);
		void SetScale(glm::vec2& scale);

		void SetFontBatch(FontBatch* fb) {
			m_fontBatch = fb;
		}

		void SetText(std::string text) {
			m_text = text;
		}

		std::string GetText() {
			return m_text;
		}

		void SetJustification(Justification just) {
			m_justification = just;
		}

		Justification GetJustification() {
			return m_justification;
		}

		void SetColor(GLubyte r, GLubyte g, GLubyte b, GLubyte a = 255) {
			m_color.R = r;
			m_color.G = g;
			m_color.B = b;
			m_color.A = a;
		}

	private:
		Justification m_justification = Justification::LEFT;
		FontBatch* m_fontBatch;
		glm::vec2 m_offset = glm::vec2(0.0f), m_scale = glm::vec2(1.0f);
		std::string m_text;
		ColorRGBA8 m_color = {255, 255, 255, 255};
		float m_depth = 10.0f;
	};
}