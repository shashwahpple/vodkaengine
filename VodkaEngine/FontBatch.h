#pragma once
#include "Utilities.h"
#include <TTF\SDL_ttf.h>
#include <glm\glm.hpp>
#include <vector>
#include "Vertex.h"

namespace VodkaEngine {
	class GLTexture;
	class Spritebatch;

	struct VODKA_API CharGlyph {
		char character;
		glm::vec4 uvRect;
		glm::vec2 size;
	};

#define FIRST_PRINTABLE_CHAR ((char)32)
#define LAST_PRINTABLE_CHAR ((char)126)

	enum class VODKA_API Justification {
		LEFT, MIDDLE, RIGHT
	};

	class VODKA_API FontBatch {
	public:
		FontBatch(const char* font, int size, char cs, char ce);
		FontBatch(const char* font, int size) : FontBatch(font, size, FIRST_PRINTABLE_CHAR, LAST_PRINTABLE_CHAR) {}
		~FontBatch() {
			Dispose();
		}

		void Dispose();

		int GetFontHeight() {
			return m_fontHeight;
		}

		glm::vec2 Measure(const char* s);

		void Draw(Spritebatch& batch, const char* s, glm::vec2 position, glm::vec2 scaling,
			float depth, ColorRGBA8 tint, Justification just = Justification::LEFT);

	private:
		static std::vector<int>* CreateRows(glm::ivec4* rects, int rectsLength, int r, int padding, int& w);

		int m_regStart, m_regLength;
		CharGlyph* m_glyphs;
		int m_fontHeight;
		unsigned int m_texID;

	};
}