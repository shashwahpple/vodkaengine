#pragma once
#include "Utilities.h"
#include "IBehaviour.h"
#include "GLTexture.h"
#include "Vertex.h"
#include "FontBatch.h"
#include "InputManager.h"

namespace VodkaEngine {
	class VODKA_API ButtonBehaviour : public IBehaviour {
	public:
		ButtonBehaviour();
		~ButtonBehaviour();

		virtual void Start() override;
		virtual void Draw(Spritebatch* spritebatch) override;
		virtual void Update(float deltaTime) final;

		void SetTexture(std::string file);
		void SetTextOffset(glm::vec2& pos);
		void SetButtonSize(glm::vec2& size);
		void SetDepth(float depth);
		void SetTextScale(glm::vec2& scale);
		void SetTextColor(GLubyte r, GLubyte g, GLubyte b, GLubyte a = 255);
		void SetSpriteColor(GLubyte r, GLubyte g, GLubyte b, GLubyte a = 255);

	protected:
		Justification m_justification = Justification::LEFT;
		FontBatch* m_fontBatch;
		glm::vec2 m_textOffset = glm::vec2(0.0f), m_textScale = glm::vec2(1.0f), m_spriteOffset = glm::vec2(0.0f), m_buttonSize = glm::vec2(64.0f);
		std::string m_text;
		ColorRGBA8 m_textColor = { 255, 255, 255, 255 }, m_spriteColor = {255, 255, 255, 255};
		float m_depth = 10.0f;
		GLuint m_texture;
		InputManager* m_inputManager;

		virtual void OnClick(float deltaTime) {}
	};
}