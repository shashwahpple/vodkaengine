#pragma once
#include <GL\glew.h>
#include "Utilities.h"

namespace VodkaEngine {
	struct VODKA_API GLTexture {
		GLuint id;
		int height, width;
	};
}