#include "TextBehaviour.h"
#include "FontBatch.h"

namespace VodkaEngine {
	TextBehaviour::TextBehaviour(FontBatch* fontBatch) {
		m_fontBatch = (m_fontBatch == nullptr) ? nullptr : fontBatch;
		m_color = { 255, 255, 255, 255 };
	}

	TextBehaviour::~TextBehaviour() {

	}

	void TextBehaviour::Draw(Spritebatch * spritebatch) {

		m_fontBatch->Draw(*spritebatch, m_text.c_str(), m_owner->GetTransform().GetGlobalPosition() + m_offset, m_scale, m_depth, m_color, m_justification);

	}

	void TextBehaviour::SetOffset(glm::vec2 & pos) {
		m_offset = pos;
	}

	void TextBehaviour::SetScale(glm::vec2 & scale) {
		m_scale = scale;
	}
	
}