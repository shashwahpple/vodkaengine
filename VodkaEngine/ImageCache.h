#pragma once
#include <map>
#include "Utilities.h"
#include "GLTexture.h"

namespace VodkaEngine {
	class VODKA_API ImageCache {
	public:
		ImageCache();
		~ImageCache();

		GLTexture GetTexture(std::string file);
	private:
		std::map<std::string, GLTexture> m_textureMap;
	};
}