#include "IScreenHandler.h"
#include "IScreen.h"
#include "VodkaEngine.h"
#include <filesystem>
#include <iostream>

#if defined _WIN32 || defined _WIN64
#include <SDL\SDL.h>
#endif

#include <algorithm>

namespace VodkaEngine {
	IScreenHandler::~IScreenHandler() {
		for (size_t i = 0; i < m_screens.size(); i++) {
			delete m_screens[i].first;
		}

		m_fontBatch->Dispose();

		delete m_window;
		delete m_inputManager;
		delete m_fontBatch;
	}

	void IScreenHandler::Run() {
		m_engineState = EngineState::RUNNING;

		Vodka();

		Init();

		m_inputManager = new InputManager();
		m_window = new Window();

		m_window->Create(m_windowName, m_screenWidth, m_screenHeight, m_screenFlags);

		InitShaders();

		m_fontBatch = new FontBatch(m_fontPath.c_str(), m_fontSize);

		m_fpsLimiter.SetMaxFPS(m_maxFps);
		GlobalHandler::Instance()->GlobalAudioHandler->Init();

		for (unsigned int i = 0; i < m_screens.size(); i++) {
			m_screens[i].first->PInit();
		}

		for (auto p : std::experimental::filesystem::directory_iterator("Data/Images")) {
			GlobalHandler::Instance()->GlobalImageCache->GetTexture(p.path().relative_path().string());
		}

		for (auto p : std::experimental::filesystem::directory_iterator("Data/Sound/Music")) {
			GlobalHandler::Instance()->GlobalAudioHandler->LoadMusic(p.path().relative_path().string());
		}

		for (auto p : std::experimental::filesystem::directory_iterator("Data/Sound/Effects")) {
			GlobalHandler::Instance()->GlobalAudioHandler->LoadSoundEffect(p.path().relative_path().string());
		}

		while (m_engineState != EngineState::EXIT) {
			ScreenLoop();
		}
	}

	void IScreenHandler::PushScreen(std::string& screenName) {
		switch (m_pauseState) {
		case ScreenPauseState::PAUSE:
			for (size_t i = 0; i < m_screens.size(); i++) {
				if (m_screens[i].first->GetActive()) {
					m_screens[i].first->SetActive(false);
				}
			}
		case ScreenPauseState::RUN:
			for (size_t i = 0; i < m_screens.size(); i++) {
				if (m_screens[i].second == screenName) {
					if (m_screens[i].first->GetActive()) {
						return;
					}
					m_screens[i].first->SetActive(true);
				}
			}
			break;
		}
		

	}

	void IScreenHandler::PopScreen(std::string& screenName) {
		for (size_t i = 0; i < m_screens.size(); i++) {
			if (m_screens[i].second == screenName) {
				if (!m_screens[i].first->GetActive()) {
					return;
				}
				m_screens[i].first->SetActive(false);
			}
		}
	}

	void IScreenHandler::ScreenLoop() {
		//	Define values used in processing deltaTime
		const float MS_PER_SECOND = 1000.0f;
		const float DESIRED_FRAMETIME = MS_PER_SECOND / 60.0f;
		const float MAX_DELTA_TIME = 1.0f;
		const int MAX_PHYSICS_STEPS = 6;

		float previousTicks = (float)SDL_GetTicks();

		if (m_engineState == EngineState::RUNNING) {
			const int MAX_PHYSICS_STEPS = 6;

			m_fpsLimiter.Begin();

			float newTicks = (float)SDL_GetTicks();
			float frameTime = newTicks - previousTicks;

			previousTicks = newTicks;
			float totalDeltaTime = frameTime / DESIRED_FRAMETIME;

			GetInput();

			//	Update program while totalDeltaTime is still above 0 or if MAX_PHYSICS_STEPS has yet to be reached
			int i = 0;
			while (totalDeltaTime > 0.0f && i < MAX_PHYSICS_STEPS) {
				m_deltaTime = std::min(totalDeltaTime, MAX_DELTA_TIME);

				//	Run update functions on all active screens
				for (size_t j = 0; j < m_screens.size(); j++) {
					if (m_screens[j].first->GetActive()) {
						m_screens[j].first->PUpdate(m_deltaTime);
					}
				}

				totalDeltaTime -= m_deltaTime;

				i++;
			}

			//	Update all cameras in each active screen
			for (size_t j = 0; j < m_screens.size(); j++) {
				if (m_screens[j].first->GetActive()) {
					m_screens[j].first->UpdateCameras();
				}
			}

			Draw();

			//	Limit FPS and determine update speed
			m_fps = m_fpsLimiter.End();
		}
	}

	void IScreenHandler::Draw() {
		glClearDepth(1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		m_shaderProgram.Use();

		glActiveTexture(GL_TEXTURE0);
		GLuint textureUniform = m_shaderProgram.GetUniformLocation("mySampler");
		glUniform1i(textureUniform, 0);

		GlobalHandler* g = GlobalHandler::Instance();

		for (size_t i = 0; i < m_screens.size(); i++) {
			if (m_screens[i].first->GetActive()) {
				m_screens[i].first->PDraw();
			}
		}

		m_shaderProgram.Unuse();

		m_window->SwapBuffer();
	}

	void IScreenHandler::InitShaders() {
		m_shaderProgram.CompileShaders(m_vertShaderPath, m_fragShaderPath);

		m_shaderProgram.AddAttribute("vertexPosition");
		m_shaderProgram.AddAttribute("vertexColor");
		m_shaderProgram.AddAttribute("vertexUV");
		m_shaderProgram.LinkShaders();
	}

	void IScreenHandler::GetInput() {
		SDL_Event evnt;

		while (SDL_PollEvent(&evnt)) {
			switch (evnt.type) {
			case SDL_QUIT:
				m_engineState = EngineState::EXIT;
				break;
			case SDL_MOUSEMOTION:
				m_inputManager->SetMouseCoords((float)evnt.motion.x, (float)evnt.motion.y);
				break;
			case SDL_KEYDOWN:
				m_inputManager->PressKey(evnt.key.keysym.sym);
				break;
			case SDL_KEYUP:
				m_inputManager->ReleaseKey(evnt.key.keysym.sym);
				break;
			case SDL_MOUSEBUTTONDOWN:
				m_inputManager->PressKey(evnt.button.button);
				break;
			case SDL_MOUSEBUTTONUP:
				m_inputManager->PressKey(evnt.button.button);
				break;
			}
		}
	}
}