#include "ShaderProgram.h"
#include "FileIO.h"
#include "Utilities.h"
#include <vector>

namespace VodkaEngine {

	ShaderProgram::ShaderProgram() : m_attributes(0), m_programID(0), m_fragmentShaderID(0), m_vertexShaderID(0) {

	}


	ShaderProgram::~ShaderProgram() {

	}

	void ShaderProgram::CompileShaders(const std::string& vertexFile, const std::string& fragmentFile) {
		std::string fragSource;
		std::string vertSource;

		FileIO::ReadFileToBuffer(vertexFile, vertSource);
		FileIO::ReadFileToBuffer(fragmentFile, fragSource);

		CompileShadersFromSource(vertSource.c_str(), fragSource.c_str());
	}

	void ShaderProgram::CompileShadersFromSource(const char* vertSource, const char* fragSource) {
		m_programID = glCreateProgram();

		m_vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
		if (m_vertexShaderID == 0) {
			FatalError("Vertex shader failed to be created. :(");
		}

		m_fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
		if (m_fragmentShaderID == 0) {
			FatalError("Fragment shader failed to be created. :(");
		}

		CompileShader(vertSource, "Vertex Shader", m_vertexShaderID);
		CompileShader(fragSource, "Fragment Shader", m_fragmentShaderID);
	}

	void ShaderProgram::LinkShaders() {
		glAttachShader(m_programID, m_vertexShaderID);
		glAttachShader(m_programID, m_fragmentShaderID);

		glLinkProgram(m_programID);

		GLuint linkStatus = 0;
		glGetProgramiv(m_programID, GL_LINK_STATUS, (int*)&linkStatus);

		if (linkStatus == GL_FALSE) {
			GLint maxLength = 0;
			glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<GLchar> infoLog(maxLength);
			glGetProgramInfoLog(m_programID, maxLength, &maxLength, &infoLog[0]);

			glDeleteProgram(m_programID);
			glDeleteShader(m_vertexShaderID);
			glDeleteShader(m_fragmentShaderID);

			std::printf("%s\n", &(infoLog[0]));
			FatalError("Shader(s) failed to link. :(");
		}

		glDetachShader(m_programID, m_vertexShaderID);
		glDetachShader(m_programID, m_fragmentShaderID);
	}

	void ShaderProgram::CompileShader(const char* source, const std::string& name, GLuint id) {
		glShaderSource(id, 1, &source, nullptr);
		glCompileShader(id);

		GLint compileStatus = 0;
		glGetShaderiv(id, GL_COMPILE_STATUS, &compileStatus);

		if (compileStatus == GL_FALSE) {
			GLint maxLength = 0;
			glGetShaderiv(id, GL_INFO_LOG_LENGTH, &maxLength);

			std::vector<char> errorLog(maxLength);
			glGetShaderInfoLog(id, maxLength, &maxLength, &errorLog[0]);

			glDeleteShader(id);

			std::printf("%s\n", &(errorLog[0]));
			FatalError("Shader " + name + " failed to compile. :(");
		}
	}

	void ShaderProgram::AddAttribute(const std::string& attributeName) {
		glBindAttribLocation(m_programID, m_attributes++, attributeName.c_str());
	}

	GLuint ShaderProgram::GetUniformLocation(const std::string& uniformName) {
		GLint location = glGetUniformLocation(m_programID, uniformName.c_str());

		if (location == GL_INVALID_INDEX) {
			FatalError("Uniform " + uniformName + " not found in Shader. :(");
		}

		return location;
	}

	void ShaderProgram::Use() {
		glUseProgram(m_programID);
		for (unsigned int i = 0; i < m_attributes; i++) {
			glEnableVertexAttribArray(i);
		}
	}

	void ShaderProgram::Unuse() {
		glUseProgram(0);
		for (unsigned int i = 0; i < m_attributes; i++) {
			glDisableVertexAttribArray(i);
		}
	}
}