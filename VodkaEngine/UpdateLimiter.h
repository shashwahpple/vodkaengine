#pragma once
#include "Utilities.h"

namespace VodkaEngine {
	class VODKA_API UpdateLimiter {
	public:
		UpdateLimiter();
		~UpdateLimiter();

		//	SetMaxFPS sets the member variable m_maxFPS to be the value of its' parameter
		void SetMaxFPS(float maxFPS);

		void Begin();

		float End();
	
	private:
		void CalculateFPS();
		float m_maxFPS = 60.0f;
		float m_fps;
		float m_frameTime;
		unsigned int m_startTicks;
	};
}