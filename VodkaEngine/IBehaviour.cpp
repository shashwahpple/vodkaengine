#include "IBehaviour.h"

namespace VodkaEngine {
	void IBehaviour::SetOwner(GameObject* object) {
		m_owner = object;
	}

	bool IBehaviour::Active() {
		return m_active;
	}
	
	bool IBehaviour::Active(bool active) {
		m_active = active;
		return m_active;
	}
}