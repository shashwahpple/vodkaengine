#include "ImageCache.h"
#include "ResourceLoader.h"

namespace VodkaEngine {
	ImageCache::ImageCache() {
	
	}

	ImageCache::~ImageCache() {
	
	}

	GLTexture ImageCache::GetTexture(std::string file) {
		auto mit = m_textureMap.find(file);

		if (mit == m_textureMap.end()) {
			GLTexture newTex = ResourceLoader::LoadPNG(file);
			m_textureMap.insert(make_pair(file, newTex));

			return newTex;
		}

		return mit->second;
	}


}