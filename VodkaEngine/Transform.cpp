#include "Transform.h"
#include <glm\gtx\matrix_transform_2d.hpp>

namespace VodkaEngine {
	Transform::Transform() {
		m_local = glm::mat3(1.0f);
	}

	Transform::~Transform() {
	
	}

	void Transform::SetParent(Transform* parent) {
		m_parent = parent;
	}

	glm::mat3& Transform::GetTransform() {
		return m_local;
	}

	glm::mat3& Transform::GetGlobalTransform() {
		if (m_parent == nullptr) {
			return m_local;
		}

		return m_parent->GetGlobalTransform() * m_local;
	}

	void Transform::Translate(const glm::vec2& pos) {
		m_local = glm::translate(m_local, pos);
	}

	void Transform::Rotate(const float radians) {
		m_local = glm::rotate(m_local, radians);
	}

	glm::vec2 Transform::GetLocalPosition() {
		return (glm::vec2)m_local[2];
	}

	float Transform::GetLocalRotation() {
		return atan2f(m_local[1][0], m_local[0][0]);
	}

	glm::vec2 Transform::GetGlobalPosition() {
		return (glm::vec2)GetGlobalTransform()[2];
	}

	float Transform::GetGlobalRotation() {
		return atan2f(GetGlobalTransform()[1][0], GetGlobalTransform()[0][0]);
	}
}