#include "UpdateLimiter.h"

#if defined _WIN32 || defined _WIN64
#include <SDL\SDL.h>
#endif

namespace VodkaEngine {
	UpdateLimiter::UpdateLimiter() {
	
	}

	UpdateLimiter::~UpdateLimiter()	{
	
	}

	void UpdateLimiter::SetMaxFPS(float maxFPS) {
		m_maxFPS = maxFPS;
	}

	void UpdateLimiter::Begin() {
		m_startTicks = SDL_GetTicks();
	}

	float UpdateLimiter::End() {
		CalculateFPS();

		//	Calculate time since Begin was called
		float frameTicks = (float)SDL_GetTicks() - (float)m_startTicks;

		//	Delay the application if FPS will exceed MaxFPS
		if ((1000.0f / m_maxFPS) > frameTicks) {
			SDL_Delay(Uint32((1000.0f / m_maxFPS) - frameTicks));
		}

		return m_fps;
	}

	void UpdateLimiter::CalculateFPS() {
		//	Frame counter setup
		static const int NUM_SAMPLES = 10;
		static int currentFrame = 0;
		static float frameTimes[NUM_SAMPLES];
		
		//	Get time since SDL_Init was called
		static float prevTicks = (float)SDL_GetTicks();
		float currentTicks = (float)SDL_GetTicks();
		
		m_frameTime = currentFrame - prevTicks;

		prevTicks = (float)SDL_GetTicks();
		
		frameTimes[currentFrame % NUM_SAMPLES] = m_frameTime;

		int count;
		currentFrame++;

		if (currentFrame < NUM_SAMPLES) {
			count = currentFrame;
		} else {
			count = NUM_SAMPLES;
		}

		float frameTimeAvg = 0;

		for (int i = 0; i < count; i++) {
			frameTimeAvg += frameTimes[i];
		}

		frameTimeAvg /= count;

		//	Determine how fast the engine is running and calculate fps accordingly
		if (frameTimeAvg > 0) {
			m_fps = 1000.0f / frameTimeAvg;
		} else {
			m_fps = m_maxFPS;
		}
	}
}