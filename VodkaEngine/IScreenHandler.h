#pragma once
#include <string>
#include <vector>
#include "Utilities.h"
#include "UpdateLimiter.h"
#include "Window.h"
#include "InputManager.h"
#include "ShaderProgram.h"
#include "AudioHandler.h"
#include "ImageCache.h"
#include "FontBatch.h"

namespace VodkaEngine {
	enum EngineState {
		RUNNING,
		PAUSED,
		EXIT
	};

	enum ScreenPauseState {
		PAUSE,
		RUN
	};

	class IScreen;

	class VODKA_API IScreenHandler {
	public:
		IScreenHandler() { GlobalHandler::Instance()->ScreenHandler = this; GlobalHandler::Instance()->GlobalImageCache = new ImageCache(); GlobalHandler::Instance()->GlobalAudioHandler = new AudioHandler(); }
		~IScreenHandler();

		void Run();

		void PushScreen(std::string& screenName);
		void PopScreen(std::string& screenName);

		void SetEngineState(EngineState state) {
			m_engineState = state;
		}

		ShaderProgram* GetShaderProgram() {
			return &m_shaderProgram;
		}

		InputManager* GetInputManager() {
			return m_inputManager;
		}

		FontBatch* GetFontBatch() {
			return m_fontBatch;
		}

		//TODO: Move audio to GlobalHandler
		AudioHandler* GetAudio() {
			return m_audioHandler;
		}

		unsigned int GetWidth() {
			return m_screenWidth;
		}

		unsigned int GetHeight() {
			return m_screenHeight;
		}

	protected:
		//	Using std::pair to save a function call, as objects in pair can be accessed with ::first or ::second
		std::vector<std::pair<IScreen*, std::string>> m_screens;

		//	Used to limit the fps to a defined value
		UpdateLimiter m_fpsLimiter;

		//	Used to modify values for accurate processing
		float m_deltaTime;

		//	FPS and MaxFPS for accurately determining speed of engine
		float m_fps, m_maxFps;

		//	Enum for controlling engine processes
		EngineState m_engineState;
		
		//	State for screens
		ScreenPauseState m_pauseState;

		//	Used to handle shaders and rendering accordingly
		ShaderProgram m_shaderProgram;

		//	Used for batching fonts
		FontBatch* m_fontBatch;

		//	Engine window
		Window* m_window;

		//	Override function to initialise any and all variables for the class
		virtual void Init() = 0;

		//	Screen Update Loop for updating all screens, their cameras and drawing all data to the window
		void ScreenLoop();

		//	Draw function
		void Draw();

		//	Sets up shaders and attributes
		void InitShaders();

		//	Set paths for shaders
		std::string m_vertShaderPath, m_fragShaderPath;

		//	Process input from SDL
		void GetInput();

		//	Window Setup variables
		std::string m_windowName, m_fontPath;
		unsigned int m_screenWidth, m_screenHeight, m_fontSize;
		unsigned int m_screenFlags = 0b0;

		//	Object for handling input
		InputManager* m_inputManager;

		//	Handler for all music and sound effect
		AudioHandler* m_audioHandler;
	};
}