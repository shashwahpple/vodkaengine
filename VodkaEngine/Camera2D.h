#pragma once
#include "Utilities.h"
#include <glm\glm.hpp>


namespace VodkaEngine {
	class VODKA_API Camera2D {
	public:
		Camera2D();
		~Camera2D();

		void Init(int width, int height);

		void Update();

		glm::vec2 ConvertScreenToWorld(glm::vec2 coords);

		bool IsInView(const glm::vec2& position, const glm::vec2& dims);

		void SetPosition(const glm::vec2& pos) {
			m_needsMatUpdate = true;
			m_position = pos;
		}

		glm::vec2 GetPosition() {
			return m_position;
		}

		void SetScale(float scale) {
			m_needsMatUpdate = true;
			m_scale = scale;
		}

		float GetScale() {
			return m_scale;
		}

		glm::mat4 GetOrthoMatrix() {
			return m_orthoMatrix;
		}

	private: 
		float m_scale;
		bool m_needsMatUpdate;
		glm::ivec2 m_screenSize;
		glm::vec2 m_position;
		glm::mat4 m_cameraMatrix;
		glm::mat4 m_orthoMatrix;
	};
}