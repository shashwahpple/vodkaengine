#pragma once
#include "Utilities.h"
#include "GameObject.h"

namespace VodkaEngine {
	class Spritebatch;
	class GameObject;

	class VODKA_API IBehaviour {
	public:
		virtual void Start() {}
		virtual void OnActive() {}
		virtual void Update(float deltaTime) {}
		virtual void Draw(Spritebatch* spritebatch) {}
		virtual void End() {}

		void SetOwner(GameObject* object);

		bool Active();
		bool Active(bool active);

	protected:
		bool m_active = true;
		GameObject* m_owner;
	};
}