#pragma once
#include "Utilities.h"
#include <GL\glew.h>
#include <string>

namespace VodkaEngine {


	class VODKA_API ShaderProgram {
	public:
		ShaderProgram();
		~ShaderProgram();

		//	Read shader files and compile the source code
		void CompileShaders(const std::string& vertexFile, const std::string& fragmentFile);
		void CompileShadersFromSource(const char* vertSource, const char* fragSource);

		//	Takes shaders and links them
		void LinkShaders();

		//	Enables or disables Vertex Attributes
		void Use();
		void Unuse();

		//	Returns location of Shader Uniform (if found)
		GLuint GetUniformLocation(const std::string& uniformName);

		// Adds attribute to program
		void AddAttribute(const std::string& attributeName);

	private:
		//	Number of available shader attributes
		unsigned int m_attributes;

		//	Compiles shader code for future use
		void CompileShader(const char* source, const std::string& name, GLuint id);

		//	Locations for the Program and the two main shaders
		GLuint m_programID;
		GLuint m_vertexShaderID;
		GLuint m_fragmentShaderID;
	};
}