#include "SpriteBehaviour.h"
#include "ResourceLoader.h"
#include "Spritebatch.h"
#include "ImageCache.h"

namespace VodkaEngine {
	void SpriteBehaviour::Draw(Spritebatch* spritebatch) {
		const glm::vec4 uvRect = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);

		glm::vec2 pos = m_owner->GetTransform().GetGlobalPosition();
		glm::vec4 dest;
		dest.x = pos.x + m_offset.x;
		dest.y = pos.y + m_offset.y;
		dest.z = m_width * m_scale.x;
		dest.w = m_height * m_scale.y;

		spritebatch->Draw(dest, uvRect, m_texture, m_depth, m_color, m_rotation);
	}

	void SpriteBehaviour::SetTexture(std::string file) {
		GLTexture tex = GlobalHandler::Instance()->GlobalImageCache->GetTexture(file);
		m_texture = tex.id;
		m_width = tex.width;
		m_height = tex.height;
	}

	void SpriteBehaviour::SetOffset(glm::vec2& pos) {
		m_offset = pos;
	}

	void SpriteBehaviour::SetDepth(float depth) {
		m_depth = depth;
	}

	void SpriteBehaviour::SetRotation(float radians) {
		m_rotation = radians;
	}

	void SpriteBehaviour::SetScale(glm::vec2& scale) {
		m_scale = scale;
	}

	void SpriteBehaviour::SetColor(GLubyte r, GLubyte g, GLubyte b, GLubyte a) {
		m_color = { r, g, b, a };
	}
}