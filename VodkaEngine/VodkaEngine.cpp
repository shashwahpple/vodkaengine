#include "VodkaEngine.h"

#if defined _WIN32 || defined _WIN64
#include <SDL\SDL.h>
#include <GL\glew.h>
#else
#error "Unknown Platform"
#endif

namespace VodkaEngine {
	int Vodka() {

		//	Makes SDL Initialise all modules for later use in the engine
		SDL_Init(SDL_INIT_EVERYTHING);

		//	Forces SDL and OpenGL to use Double Buffering, 1 turns it on, 0 turns it off
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

		return 0;
	}
}