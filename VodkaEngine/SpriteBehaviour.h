#pragma once
#include "Utilities.h"
#include "IBehaviour.h"
#include "GLTexture.h"
#include "Vertex.h"

namespace VodkaEngine {
	class VODKA_API SpriteBehaviour : public IBehaviour {
	public:
		SpriteBehaviour() {}
		~SpriteBehaviour() {}

		virtual void Draw(Spritebatch* spritebatch) override;

		void SetTexture(std::string file);
		void SetOffset(glm::vec2& pos);
		void SetDepth(float depth);
		void SetRotation(float radians);
		void SetScale(glm::vec2& scale);
		void SetColor(GLubyte r, GLubyte g, GLubyte b, GLubyte a = 255);
		
	protected:
		glm::vec2 m_offset = glm::vec2(0.0f), m_scale = glm::vec2(1.0f);
		GLuint m_texture;
		ColorRGBA8 m_color = { 255, 255, 255, 255 };
		float m_depth = 0.0f, m_rotation = 0.0f;
		float m_height = 0.0f, m_width = 0.0f;
	};
}