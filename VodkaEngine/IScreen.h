#pragma once
#include "Utilities.h"
#include "IScreenHandler.h"
#include "Spritebatch.h"
#include <vector>

namespace VodkaEngine {
	class Camera2D;

	class VODKA_API IScreen {
	public:
		IScreen(IScreenHandler* handler);
		//	Virtual so that pointers can be deleted on destruction of object
		virtual ~IScreen();

		void PInit();

		//	Enables or disables screen updates
		void SetActive(bool b) { m_isActive = b; }
		bool GetActive() { return m_isActive; }

		void UpdateCameras();

		void PUpdate(float deltaTime);
		void PDraw();

	protected:
		virtual void Init() = 0;
		virtual void Draw() = 0;

		//	Updates for physics, cameras and drawing functionality
		virtual void Update(float deltaTime) = 0;

		bool m_isActive = true;
		IScreenHandler* m_screenHandler;
		Spritebatch* m_uiSpritebatch;

		std::vector<GameObject*> m_uiObjects;
		std::vector<Camera2D*> m_activeCameras;
	};
}