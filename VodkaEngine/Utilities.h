#pragma once
#include <string>
#include <vector>

namespace VodkaEngine {
#ifdef VODKA_EXPORTS  
#define VODKA_API __declspec(dllexport)  
#define VODKA_TEMPLATE
#else  
#define VODKA_API __declspec(dllimport)
#define VODKA_TEMPLATE extern
#endif  



	class IScreenHandler;
	class ImageCache;
	class AudioHandler;
	class GameObject;

	typedef unsigned int uint;

	class VODKA_API GlobalHandler {
	public:
			
		static GlobalHandler* Instance() {
			if (m_instance == nullptr) {
				m_instance = new GlobalHandler();
			}

			return m_instance;
		}

		IScreenHandler* ScreenHandler;
		ImageCache* GlobalImageCache;
		AudioHandler* GlobalAudioHandler;
		std::vector<GameObject*> PersistentObjects;

		void Unuse() {
			delete m_instance;
			delete GlobalImageCache;
			delete GlobalAudioHandler;

			for (unsigned int i = 0; i < PersistentObjects.size(); i++) {
				delete PersistentObjects[i];
			}
		}

	private:
		static GlobalHandler* m_instance;
	};

	extern void VODKA_API FatalError(std::string errorString);
}