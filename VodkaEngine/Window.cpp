#include "Window.h"


namespace VodkaEngine {
	Window::~Window() {
		SDL_DestroyWindow(m_sdlWindow);
	}

	unsigned int Window::Create(std::string& windowName, int screenWidth, int screenHeight, unsigned int currentFlags) {
		Uint32 flags = SDL_WINDOW_OPENGL;

		if (currentFlags & INVISIBLE) {
			flags |= SDL_WINDOW_HIDDEN;
		}

		if (currentFlags & FULLSCREEN) {
			flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
		}

		if (currentFlags & BORDERLESS) {
			flags |= SDL_WINDOW_BORDERLESS;
		}

		m_sdlWindow = SDL_CreateWindow(windowName.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, screenWidth, screenHeight, flags);

		if (m_sdlWindow == nullptr) {
			//	TODO: Error handling for window failed to create
			FatalError("SDL Window Failed to create.");
		}

		SDL_GLContext glContext = SDL_GL_CreateContext(m_sdlWindow);
		if (glContext == nullptr) {
			//	TODO: Error handling for OpenGL Context failed to create
			FatalError("OpenGL Context Failed to create.");
		}

		GLenum error = glewInit();
		if (error != GLEW_OK) {
			//	TODO: Error handling for Glew failed to initialise
			FatalError("Glew failed to initialise.");
		}

		glClearColor(0.75f, 0.0f, 0.75f, 1.0f);

		//	Forces Engine to use VSync
		SDL_GL_SetSwapInterval(1);

		glOrtho(0.0, 0.0, screenWidth, screenHeight, -1, 1);

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		//	TODO: TEST THIS LINE
		//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		return 0;
	}
}