#pragma once
#include <glm\glm.hpp>
#include "Utilities.h"

namespace VodkaEngine {
	class VODKA_API Transform	{
	public:
		Transform();
		virtual ~Transform();

		void SetParent(Transform* parent);

		glm::mat3& GetTransform();
		glm::mat3& GetGlobalTransform();

		void Translate(const glm::vec2& pos);
		void Rotate(const float radians);

		glm::vec2 GetLocalPosition();
		float GetLocalRotation();

		glm::vec2 GetGlobalPosition();
		float GetGlobalRotation();

	protected:
		glm::mat3 m_local;
		Transform* m_parent = nullptr;
	};
}