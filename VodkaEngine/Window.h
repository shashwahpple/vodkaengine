#pragma once
#include "Utilities.h"

#include <SDL\SDL.h>
#include <GL\glew.h>
#include <string>

namespace VodkaEngine {

	enum WindowFlags {
		NONE = 0b0,
		INVISIBLE = 0b1,
		FULLSCREEN = 0b10,
		BORDERLESS = 0b100
	};

	class VODKA_API Window {
	public:
		Window() {}
		~Window();

		unsigned int Create(std::string& windowName, int screenWidth, int screenHeight, unsigned int currentFlags);

		void SwapBuffer() { SDL_GL_SwapWindow(m_sdlWindow); }

		unsigned int GetScreenWidth() { return m_screenWidth; }
		unsigned int GetScreenHeight() { return m_screenHeight; }

	private:
		SDL_Window* m_sdlWindow;
		unsigned int m_screenWidth;
		unsigned int m_screenHeight;
	};

}