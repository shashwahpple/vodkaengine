#include "IScreen.h"
#include "Camera2D.h"
#include "GameObject.h"

namespace VodkaEngine {
	IScreen::IScreen(IScreenHandler* handler) {
		m_screenHandler = handler;
	}

	IScreen::~IScreen() {
		for (size_t i = 0; i < m_activeCameras.size(); i++) {
			delete m_activeCameras[i];
		}
	}

	void IScreen::UpdateCameras() {
		for (size_t i = 0; i < m_activeCameras.size(); i++) {
			m_activeCameras[i]->Update();
		}
	}

	void IScreen::PUpdate(float deltaTime) {
		for (unsigned int i = 0; i < m_uiObjects.size(); i++) {
			m_uiObjects[i]->Update(deltaTime);
		}

		Update(deltaTime);
	}

	void IScreen::PInit() {
		m_uiSpritebatch = new Spritebatch();
		m_uiSpritebatch->Init();

		Camera2D* uiCamera = new Camera2D();
		uiCamera->Init(GlobalHandler::Instance()->ScreenHandler->GetWidth(), GlobalHandler::Instance()->ScreenHandler->GetHeight());
		m_activeCameras.emplace_back(uiCamera);

		Init();
	}
	
	void IScreen::PDraw() {
		glm::mat4 projectionMatrix = m_activeCameras[0]->GetOrthoMatrix();
		GLuint pUniform = m_screenHandler->GetShaderProgram()->GetUniformLocation("P");
		glUniformMatrix4fv(pUniform, 1, GL_FALSE, &projectionMatrix[0][0]);

		Draw();

		glm::mat4 projectionMatrix = m_activeCameras[1]->GetOrthoMatrix();
		GLuint pUniform = m_screenHandler->GetShaderProgram()->GetUniformLocation("P");
		glUniformMatrix4fv(pUniform, 1, GL_FALSE, &projectionMatrix[0][0]);

		m_uiSpritebatch->Begin(GlyphSortType::BACK_TO_FRONT);

		for (unsigned int i = 0; i < m_uiObjects.size(); i++) {
			m_uiObjects[i]->Draw(m_uiSpritebatch);
		}

		m_uiSpritebatch->End();
		m_uiSpritebatch->RenderBatch();
	}
}