#include "GameObject.h"
#include "Spritebatch.h"
#include "IBehaviour.h"

namespace VodkaEngine {
	GameObject::GameObject() {
		Start();
	}

	GameObject::~GameObject() {
		End();

		for (size_t i = 0; i < m_behaviours.size(); i++) {
			if (m_behaviours[i] != nullptr)
				if (m_behaviours[i]->Active())
					delete m_behaviours[i];
		}
	}

	void GameObject::Start() {
		for (size_t i = 0; i < m_behaviours.size(); i++) {
			if (m_behaviours[i] != nullptr)
				if (m_behaviours[i]->Active())
					m_behaviours[i]->Start();
		}
	}

	void GameObject::OnActive() {
		for (size_t i = 0; i < m_behaviours.size(); i++) {
			if (m_behaviours[i] != nullptr)
				if (m_behaviours[i]->Active())
					m_behaviours[i]->OnActive();
		}
	}

	void GameObject::Update(float deltaTime) {
		if (m_active) {
			for (size_t i = 0; i < m_behaviours.size(); i++) {
				if (m_behaviours[i] != nullptr)
					if (m_behaviours[i]->Active())
						m_behaviours[i]->Update(deltaTime);
			}
		}
	}

	void GameObject::Draw(Spritebatch* spritebatch) {
		for (size_t i = 0; i < m_behaviours.size(); i++) {
			if (m_behaviours[i] != nullptr)
				if (m_behaviours[i]->Active())
					m_behaviours[i]->Draw(spritebatch);
		}
	}

	void GameObject::End() {
		for (size_t i = 0; i < m_behaviours.size(); i++) {
			if (m_behaviours[i] != nullptr)
				if (m_behaviours[i]->Active())
					m_behaviours[i]->End();
		}
	}

	void GameObject::Active(bool state) {
		if (state) {
			OnActive();
		}

		m_active = state;
	}

	IBehaviour* GameObject::AddBehaviour(IBehaviour* bev) {
		bev->SetOwner(this);
		m_behaviours.push_back(bev);
		return bev;
	}
}