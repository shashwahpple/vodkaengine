#include "Camera2D.h"
#include <glm\gtc\matrix_transform.hpp>

namespace VodkaEngine {
	Camera2D::Camera2D() :
	m_position(0.0f, 0.0f),
	m_cameraMatrix(1.0f),
	m_orthoMatrix(1.0f),
	m_scale(1.0f),
	m_needsMatUpdate(true),
	m_screenSize(0, 0){
	
	}

	Camera2D::~Camera2D() {

	}

	void Camera2D::Update() {
		if (m_needsMatUpdate) {
			glm::vec3 translate(-m_position.x + float(m_screenSize.x / 2), -m_position.y + float(m_screenSize.y / 2), 0.0f);
			m_cameraMatrix = glm::translate(m_orthoMatrix, translate);

			glm::vec3 scale(m_scale, m_scale, 0.0f);
			m_cameraMatrix = glm::scale(glm::mat4(1.0f), scale) * m_cameraMatrix;

			m_needsMatUpdate = false;
		}
	}

	glm::vec2 Camera2D::ConvertScreenToWorld(glm::vec2 coords) {
		coords.y = m_screenSize.y - coords.y;
		coords -= glm::vec2(m_screenSize.x / 2, m_screenSize.y / 2);
		coords /= m_scale;
		coords += m_position;
		return coords;
	}

	bool Camera2D::IsInView(const glm::vec2& position, const glm::vec2& dims) {
		glm::vec2 scaledDims = glm::vec2(m_screenSize.x, m_screenSize.y) / m_scale;

		const float MIN_DIST_X = dims.x / 2.0f + scaledDims.x / 2.0f;
		const float MIN_DIST_Y = dims.y / 2.0f + scaledDims.y / 2.0f;

		glm::vec2 centerPos = position + dims / 2.0f;
		glm::vec2 cameraPos = m_position;
		glm::vec2 distVec = centerPos - cameraPos;

		float xDepth = MIN_DIST_X - abs(distVec.x);
		float yDepth = MIN_DIST_Y - abs(distVec.y);

		if (xDepth > 0.0f || yDepth > 0.0f) {
			return true;
		}

		return false;
	}

	void Camera2D::Init(int width, int height) {
		m_screenSize = glm::vec2(width, height);
		m_orthoMatrix = glm::ortho(0.0f, (float)width, 0.0f, (float)height);
	}
}