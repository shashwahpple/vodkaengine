#include "Utilities.h"

#include <assert.h>
#include <conio.h>
#include <iostream>
#include "IScreenHandler.h"

namespace VodkaEngine {
	void FatalError(std::string errorString) {
		std::cout << errorString << std::endl;
		std::printf("Enter a key to exit!");
		_getch();
		system("pause");
		GlobalHandler::Instance()->ScreenHandler->SetEngineState(EngineState::EXIT);
	}

	GlobalHandler* GlobalHandler::m_instance;
}