#pragma once
#include "Utilities.h"
#include <string>
#include <vector>

namespace VodkaEngine {
	class VODKA_API FileIO {
	public:
		static bool ReadFileToBuffer(std::string filePath, std::string& buffer);
		static bool ReadFileToBuffer(std::string filePath, std::vector<unsigned char>& buffer);
	};
}