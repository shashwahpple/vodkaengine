#pragma once
#include "Utilities.h"
#include <SDL\SDL_mixer.h>

#include <string>
#include <map>

namespace VodkaEngine {
	class VODKA_API SoundEffect {
	public:
		friend class AudioHandler;

		void Play(unsigned int loops = 0, unsigned int channel = 0);

	private:
		Mix_Chunk* m_chunk = nullptr;
	};

	class VODKA_API Music {
	public:
		friend class AudioHandler;

		void Play(unsigned int loops = 0);

		static void Pause();
		static void Stop();
		static void Resume();

	private:
		Mix_Music* m_music = nullptr;
	};

	class VODKA_API AudioHandler {
	public:
		AudioHandler() {}
		~AudioHandler();

		void Init();
		void Destroy();

		void SetVolume(unsigned int volume);

		SoundEffect LoadSoundEffect(const std::string& file);
		Music LoadMusic(const std::string& file);

	private:
		std::map<std::string, Mix_Chunk*> m_effectMap;
		std::map<std::string, Mix_Music*> m_musicMap;

		bool m_Initialised = false;
	};
}