#pragma once
#include <string>
#include "Utilities.h"
#include "GLTexture.h"

namespace VodkaEngine {
	class VODKA_API ResourceLoader {
	public:
		static GLTexture LoadPNG(std::string& file);
	};
}