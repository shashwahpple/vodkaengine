#pragma once
#include "Utilities.h"
#include <unordered_map>

#include <SDL\SDL.h>
#include <glm\glm.hpp>

namespace VodkaEngine {
	class VODKA_API InputManager {
	public:
		InputManager();

		void Update();

		void PressKey(unsigned int keyId);
		void ReleaseKey(unsigned int keyId);

		void SetMouseCoords(float x, float y);

		bool IsKeyDown(unsigned int keyId);
		bool IsKeyPressed(unsigned int keyId);

		int GetKey();
		char GetCharacter();

		glm::vec2 GetMouseCoords() const { return m_mouseCoords; }

	private:
		std::unordered_map<unsigned int, bool> m_keyMap;
		std::unordered_map<unsigned int, bool> m_prevKeyMap;

		glm::vec2 m_mouseCoords;

		bool WasKeyDown(unsigned int keyId);
	};
}