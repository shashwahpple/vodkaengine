#include "AudioHandler.h"

namespace VodkaEngine {

	void SoundEffect::Play(unsigned int loops, unsigned int channel) {
		if (Mix_PlayChannel(-1, m_chunk, loops) == -1) {
			if (Mix_PlayChannel(channel, m_chunk, loops) == -1) {
				FatalError("Mix_PlayChannel error: " + std::string(Mix_GetError()));
			}
		}
	}

	void Music::Play(unsigned int loops) {
		Mix_PlayMusic(m_music, loops);
	}

	void AudioHandler::SetVolume(unsigned int volume) {
		Mix_Volume(-1, volume);
	}

	void Music::Pause() {
		Mix_PauseMusic();
	}

	void Music::Stop() {
		Mix_HaltMusic();
	}

	void Music::Resume() {
		Mix_ResumeMusic();
	}

	AudioHandler::~AudioHandler() {
		Destroy();
	}

	void AudioHandler::Init() {
		if (m_Initialised) {
			FatalError("Tried to initialise AudioHandler twice!");
		}

		if (Mix_Init(MIX_INIT_OGG) == -1) {
			FatalError("Mix_Init error: " + std::string(Mix_GetError()));
		}
		if (Mix_Init(MIX_INIT_MP3) == -1) {
			FatalError("Mix_Init error: " + std::string(Mix_GetError()));
		}
		if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 1024) == -1) {
			FatalError("Mix_OpenAudio error: " + std::string(Mix_GetError()));
		}

		m_Initialised = true;
	}

	void AudioHandler::Destroy() {
		if (m_Initialised) {
			m_Initialised = false;

			for (auto& it : m_effectMap) {
				Mix_FreeChunk(it.second);
			}

			for (auto& it : m_musicMap) {
				Mix_FreeMusic(it.second);
			}

			m_effectMap.clear();
			m_musicMap.clear();

			Mix_CloseAudio();
			Mix_Quit();
		}
	}

	SoundEffect AudioHandler::LoadSoundEffect(const std::string& file) {
		auto it = m_effectMap.find(file);

		SoundEffect effect;

		if (it == m_effectMap.end()) {
			Mix_Chunk* chunk = Mix_LoadWAV(file.c_str());

			if (chunk == nullptr) {
				FatalError("Mix_LoadWAV error: " + std::string(Mix_GetError()));
			}

			effect.m_chunk = chunk;
			m_effectMap[file] = chunk;
		} else {
			effect.m_chunk = it->second;
		}

		return effect;
	}

	Music AudioHandler::LoadMusic(const std::string& file) {
		auto it = m_musicMap.find(file);

		Music music;

		if (it == m_musicMap.end()) {
			Mix_Music* mix_music = Mix_LoadMUS(file.c_str());

			if (mix_music == nullptr) {
				FatalError("Mix_LoadMUS error: " + std::string(Mix_GetError()));
			}

			music.m_music = mix_music;
			m_musicMap[file] = mix_music;
		} else {
			music.m_music = it->second;
		}

		return music;
	}
}