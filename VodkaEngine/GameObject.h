#pragma once
#include <vector>
#include "Utilities.h"
#include "Transform.h"

namespace VodkaEngine {
	class IBehaviour;
	class Spritebatch;

	class VODKA_API GameObject {
	public:
		GameObject();
		~GameObject();

		void Start();
		void OnActive();
		void Update(float deltaTime);
		void Draw(Spritebatch* spritebatch);
		void End();

		void Active(bool active);

		IBehaviour* AddBehaviour(IBehaviour* bev);
		Transform& GetTransform() { return m_transform; }

		template <class T>
		IBehaviour* GetBehaviour() {
			for (unsigned int i = 0; i < m_behaviours.size(); i++) {
				if (auto temp = dynamic_cast<T*>(m_behaviours[i])) {
					return temp;
				}
			}
			return nullptr;
		}

		template <class T>
		int GetBehaviourIndex() {
			for (unsigned int i = 0; i < m_behaviours.size(); i++) {
				if (auto temp = dynamic_cast<T*>(m_behaviours[i])) {
					return i;
				}
			}
			return -1;
		}

		template <class T>
		GameObject* IsOfType() {
			if (auto temp = dynamic_cast<T*>(this)) {
				return temp;
			}
			return nullptr;
		}

	protected:
		bool m_active;
		Transform m_transform;
		std::vector<IBehaviour*> m_behaviours;
	};
}