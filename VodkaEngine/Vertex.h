#pragma once
#include <GL\glew.h>
#include "Utilities.h"

namespace VodkaEngine {
	struct VODKA_API ColorRGBA8 {
		ColorRGBA8() : R(0), G(0), B(0), A(0) {}
		ColorRGBA8(GLubyte r, GLubyte g, GLubyte b, GLubyte a) :
			R(r), G(g), B(b), A(a) {}

		GLubyte R;
		GLubyte G;
		GLubyte B;
		GLubyte A;
	};

	struct VODKA_API Position {
		float X;
		float Y;
	};

	struct VODKA_API UV {
		float U;
		float V;
	};

	struct VODKA_API Vertex {
		Position position;
		ColorRGBA8 color;
		UV uv;

		void SetPosition(float x, float y) {
			position.X = x;
			position.Y = y;
		}

		void SetColor(GLubyte r, GLubyte g, GLubyte b, GLubyte a) {
			color.R = r;
			color.G = g;
			color.B = b;
			color.A = a;
		}

		void SetUV(float u, float v) {
			uv.U = u;
			uv.V = v;
		}
	};
}