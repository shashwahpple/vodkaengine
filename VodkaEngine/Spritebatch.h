#pragma once
#include <vector>
#include <GL\glew.h>
#include <glm\glm.hpp>
#include "Vertex.h"
#include "Utilities.h"

namespace VodkaEngine {
	enum class VODKA_API GlyphSortType {
		NONE,
		FRONT_TO_BACK,
		BACK_TO_FRONT,
		TEXTURE
	};

	class VODKA_API Glyph {
	public:
		Glyph(const glm::vec4& dest, const glm::vec4& uv, GLuint texture, float depth, const ColorRGBA8& color);
		Glyph(const glm::vec4& dest, const glm::vec4& uv, GLuint texture, float depth, const ColorRGBA8& color, float angle);

		GLuint texture;
		float depth;

		Vertex topLeft;
		Vertex bottomLeft;
		Vertex topRight;
		Vertex bottomRight;

	private:
		glm::vec2 RotatePoint(glm::vec2 pos, float angle);
	};

	class VODKA_API Renderbatch {
	public:
		Renderbatch(GLuint offset, GLuint numVertices, GLuint texture) : offset(offset),
			numVertices(numVertices), texture(texture) {}

		GLuint offset;
		GLuint numVertices;
		GLuint texture;
	};

	class VODKA_API Spritebatch {
	public:
		Spritebatch();
		~Spritebatch();

		void Init();

		void Begin(GlyphSortType sortType = GlyphSortType::TEXTURE);
		void End();

		void Draw(const glm::vec4& dest, const glm::vec4& uv, GLuint texture, float depth, const ColorRGBA8& color);
		void Draw(const glm::vec4& dest, const glm::vec4& uv, GLuint texture, float depth, const ColorRGBA8& color, float angle);
		void Draw(const glm::vec4& dest, const glm::vec4& uv, GLuint texture, float depth, const ColorRGBA8& color, const glm::vec2& dir);

		void RenderBatch();

	private:
		void CreateRenderBatches();
		void CreateVertexArray();
		void SortGlyphs();

		static bool CompareFrontToBack(Glyph* a, Glyph* b);
		static bool CompareBackToFront(Glyph* a, Glyph* b);
		static bool CompareTexture(Glyph* a, Glyph* b);

		GLuint m_vertBufferObj;
		GLuint m_vertArrayObj;

		GlyphSortType m_sortType;
		
		std::vector<Glyph*> m_glyphPointers;
		
		std::vector<Glyph> m_glyphs;
		std::vector<Renderbatch> m_renderBatches;
	};
}