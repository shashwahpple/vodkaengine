#include "ButtonBehaviour.h"
#include "Spritebatch.h"
#include "ImageCache.h"
#include "IScreenHandler.h"

namespace VodkaEngine {
	ButtonBehaviour::ButtonBehaviour() {

	}

	ButtonBehaviour::~ButtonBehaviour()	{
		
	}

	void ButtonBehaviour::Start() {
		m_inputManager = GlobalHandler::Instance()->ScreenHandler->GetInputManager();
	}

	void ButtonBehaviour::Draw(Spritebatch* spritebatch) {
		m_fontBatch->Draw(*spritebatch, m_text.c_str(), m_owner->GetTransform().GetGlobalPosition() + m_textOffset, m_textScale, m_depth, m_textColor, m_justification);

		if (m_texture) {
			const glm::vec4 uvRect = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);

			glm::vec2 pos = m_owner->GetTransform().GetGlobalPosition();
			glm::vec4 dest;
			dest.x = pos.x + m_spriteOffset.x;
			dest.y = pos.y + m_spriteOffset.y;
			dest.z = m_buttonSize.x;
			dest.w = m_buttonSize.y;

			spritebatch->Draw(dest, uvRect, m_texture, m_depth + 0.01f , m_spriteColor, 0.0f);
		}
	}

	void ButtonBehaviour::Update(float deltaTime) {
		glm::vec2 coords = m_inputManager->GetMouseCoords();
		glm::vec2 pos = m_owner->GetTransform().GetGlobalPosition();

		if (coords.x > pos.x && coords.x < (pos.x + m_buttonSize.x)) {
			if (coords.y > pos.y && coords.y < (pos.y + m_buttonSize.y)) {
				OnClick(deltaTime);
			}
		}
	}

	void ButtonBehaviour::SetTexture(std::string file) {
		m_texture = GlobalHandler::Instance()->GlobalImageCache->GetTexture(file).id;
	}
	
	void ButtonBehaviour::SetTextOffset(glm::vec2& pos) {
		m_textOffset = pos;
	}
	
	void ButtonBehaviour::SetButtonSize(glm::vec2& size) {
		m_buttonSize = size;
	}
	
	void ButtonBehaviour::SetDepth(float depth) {
		m_depth;
	}
	
	void ButtonBehaviour::SetTextScale(glm::vec2& scale) {
		m_textScale = scale;
	}
	
	void ButtonBehaviour::SetTextColor(GLubyte r, GLubyte g, GLubyte b, GLubyte a) {
		m_textColor = { r, g, b, a };
	}
	
	void ButtonBehaviour::SetSpriteColor(GLubyte r, GLubyte g, GLubyte b, GLubyte a) {
		m_spriteColor = { r, g, b, a };
	}
}