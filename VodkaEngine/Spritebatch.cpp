#include "Spritebatch.h"
#include <algorithm>

namespace VodkaEngine {

	Glyph::Glyph(const glm::vec4& dest, const glm::vec4& uv, GLuint texture, float depth, const ColorRGBA8& color){
		this->texture = texture;
		this->depth = depth;

		topLeft.color = color;
		topLeft.SetPosition(dest.x, dest.y + dest.w);
		topLeft.SetUV(uv.x, uv.y + uv.w);

		bottomLeft.color = color;
		bottomLeft.SetPosition(dest.x, dest.y);
		bottomLeft.SetUV(uv.x, uv.y);

		bottomRight.color = color;
		bottomRight.SetPosition(dest.x + dest.z, dest.y);
		bottomRight.SetUV(uv.x + uv.z, uv.y);

		topRight.color = color;
		topRight.SetPosition(dest.x + dest.z, dest.y + dest.w);
		topRight.SetUV(uv.x + uv.z, uv.y + uv.w);
	}

	Glyph::Glyph(const glm::vec4& dest, const glm::vec4& uv, GLuint texture, float depth, const ColorRGBA8& color, float angle) {
		this->texture = texture;
		this->depth = depth;
		glm::vec2 halfDims(dest.z / 2.0f, dest.w / 2.0f);

		glm::vec2 tl(-halfDims.x, halfDims.y);
		glm::vec2 bl(-halfDims.x, -halfDims.y);
		glm::vec2 tr(halfDims.x, halfDims.y);
		glm::vec2 br(halfDims.x, -halfDims.y);

		tl = RotatePoint(tl, angle) + halfDims;
		bl = RotatePoint(bl, angle) + halfDims;
		tr = RotatePoint(tr, angle) + halfDims;
		br = RotatePoint(br, angle) + halfDims;

		topLeft.color = color;
		topLeft.SetPosition(dest.x + tl.x, dest.y + tl.y);
		topLeft.SetUV(uv.x, uv.y + uv.w);

		bottomLeft.color = color;
		bottomLeft.SetPosition(dest.x + bl.x, dest.y + bl.y);
		bottomLeft.SetUV(uv.x, uv.y);

		bottomRight.color = color;
		bottomRight.SetPosition(dest.x + br.x, dest.y + br.y);
		bottomRight.SetUV(uv.x + uv.z, uv.y);

		topRight.color = color;
		topRight.SetPosition(dest.x + tr.x, dest.y + tr.y);
		topRight.SetUV(uv.x + uv.z, uv.y + uv.w);
	}

	glm::vec2 Glyph::RotatePoint(glm::vec2 pos, float angle) {
		glm::vec2 newv;
		newv.x = pos.x * cos(angle) - pos.y * sin(angle);
		newv.y = pos.x * sin(angle) + pos.y * cos(angle);

		return newv;
	}

	Spritebatch::Spritebatch() : m_vertBufferObj(0), m_vertArrayObj(0)	{}


	Spritebatch::~Spritebatch() {}

	void Spritebatch::Init() {
		CreateVertexArray();
	}

	void Spritebatch::Begin(GlyphSortType sortType) {
		m_sortType = sortType;
		m_renderBatches.clear();

		m_glyphs.clear();
	}

	void Spritebatch::End() {
		m_glyphPointers.resize(m_glyphs.size());

		for (unsigned int i = 0; i < m_glyphs.size(); i++) {
			m_glyphPointers[i] = &m_glyphs[i];
		}

		SortGlyphs();
		CreateRenderBatches();
	}

	void Spritebatch::Draw(const glm::vec4& dest, const glm::vec4& uv, GLuint texture, float depth, const ColorRGBA8& color) {
		m_glyphs.emplace_back(dest, uv, texture, depth, color);
	}

	void Spritebatch::Draw(const glm::vec4& dest, const glm::vec4& uv, GLuint texture, float depth, const ColorRGBA8& color, float angle) {
		m_glyphs.emplace_back(dest, uv, texture, depth, color, angle);
	}

	void Spritebatch::Draw(const glm::vec4& dest, const glm::vec4& uv, GLuint texture, float depth, const ColorRGBA8& color, const glm::vec2& dir) {
		const glm::vec2 right(1.0f, 0.0f);

		float angle = acos(glm::dot(right, dir));

		if (dir.y < 0.0f) angle = -angle;

		m_glyphs.emplace_back(dest, uv, texture, depth, color, angle);
	}

	void Spritebatch::RenderBatch() {
		glBindVertexArray(m_vertArrayObj);

		for (unsigned int i = 0; i < m_renderBatches.size(); i++) {
			glBindTexture(GL_TEXTURE_2D, m_renderBatches[i].texture);

			glDrawArrays(GL_TRIANGLES, m_renderBatches[i].offset, m_renderBatches[i].numVertices);
		}

		glBindVertexArray(0);
	}

	void Spritebatch::CreateRenderBatches() {
		std::vector<Vertex> vertices;

		vertices.resize(m_glyphPointers.size() * 6);

		if (m_glyphPointers.empty()) {
			return;
		}

		int offset = 0;
		int currentVert = 0;
	
		//	Emplace new vertices in batches of 6 (two tri's for each image)
		for (unsigned int currentGlyph = 0; currentGlyph < m_glyphPointers.size(); currentGlyph++) {
			if ((currentGlyph > 0) && (m_glyphPointers[currentGlyph]->texture == m_glyphPointers[currentGlyph - 1]->texture)){
				//	If in the current batch, increaste the number of vertices
				m_renderBatches.back().numVertices += 6;
			} else {
				//	Create a new batch
				m_renderBatches.emplace_back(offset, 6, m_glyphPointers[currentGlyph]->texture);
			}

			vertices[currentVert++] = m_glyphPointers[currentGlyph]->topLeft;
			vertices[currentVert++] = m_glyphPointers[currentGlyph]->bottomLeft;
			vertices[currentVert++] = m_glyphPointers[currentGlyph]->bottomRight;
			vertices[currentVert++] = m_glyphPointers[currentGlyph]->bottomRight;
			vertices[currentVert++] = m_glyphPointers[currentGlyph]->topRight;
			vertices[currentVert++] = m_glyphPointers[currentGlyph]->topLeft;
			offset += 6;
		}

		//	Bind the Buffer Object
		glBindBuffer(GL_ARRAY_BUFFER, m_vertBufferObj);
	
		//	Orphan the buffer for extra speed
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), nullptr, GL_DYNAMIC_DRAW);

		//	Upload data to GL buffer
		glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof(Vertex), vertices.data());

		//	Unbind Buffer Object
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void Spritebatch::CreateVertexArray() {
		//	Create Array Object if needed
		if (m_vertArrayObj == 0) {
			glGenVertexArrays(1, &m_vertArrayObj);
		}

		//	Bind Array Object, all subsequent GL calls will modify it
		glBindVertexArray(m_vertArrayObj);

		//	Generate Buffer Object if needed
		if (m_vertBufferObj == 0) {
			glGenBuffers(1, &m_vertBufferObj);
		}

		//	Bind Array Object
		glBindBuffer(GL_ARRAY_BUFFER, m_vertBufferObj);

		//	Enable Attribute Arrays for modification
		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		//	Position attribute pointer
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, position)); //TODO: make Vertex Position have z value and change second parameter of function to be 3 instead of 2

		//	Color attribute pointer
		glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Vertex), (void*) offsetof(Vertex, color));

		//	UV attribute pointer
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*) offsetof(Vertex, uv));

		//	Unbind Array Object
		glBindVertexArray(0);
	}

	void Spritebatch::SortGlyphs() {
		//	Sort glyphs based on current sort type
		switch (m_sortType) {
		case GlyphSortType::BACK_TO_FRONT:
			std::stable_sort(m_glyphPointers.begin(), m_glyphPointers.end(), CompareBackToFront);
			break;
		case GlyphSortType::FRONT_TO_BACK:
			std::stable_sort(m_glyphPointers.begin(), m_glyphPointers.end(), CompareFrontToBack);
			break;
		case GlyphSortType::TEXTURE:
			std::stable_sort(m_glyphPointers.begin(), m_glyphPointers.end(), CompareTexture);
			break;
		default:
			break;
		}
	}

	bool Spritebatch::CompareFrontToBack(Glyph* a, Glyph* b) {
		return (a->depth < b->depth);
	}

	bool Spritebatch::CompareBackToFront(Glyph* a, Glyph* b) {
		return (a->depth > b->depth);
	}

	bool Spritebatch::CompareTexture(Glyph* a, Glyph* b) {
		return (a->texture < b->texture);
	}
}